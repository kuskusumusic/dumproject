/**
 * View Models used by Spring MVC REST controllers.
 */
package com.id.dumpro.gateway.web.rest.vm;
